from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie, vary_on_headers

from rest_framework import status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
)


from goal.models import Goal, File, Stage

from goal.api.serializers import (
    GoalSerializer,
    StageSerializer,
    FileSerializer,
)


class StageViewSet(
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
    GenericViewSet
):
    queryset = Stage.objects.all()
    serializer_class = StageSerializer
    permission_classes = [permissions.IsAuthenticated]


class GoalViewSet(
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    queryset = Goal.objects.all()
    serializer_class = GoalSerializer
    permission_classes = [permissions.IsAuthenticated]
    #
    # @action(methods=["post"], detail=True)
    # def create_stage(self, request, pk=None):
    #     goal = self.get_object()
    #
    #     serializer = StageSerializer(data=request.data, context={"goal": goal})
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #
    #     return Response({"data": serializer.data, "status": status.HTTP_201_CREATED})
    #
    # @action(methods=["patch"], detail=True)
    # def update_stage(self, request, pk=None):
    #     stage_id = request.GET.get("pk")
    #
    #     if stage_id:
    #         stage = Stage.objects.filter(pk=stage_id).first()
    #
    #         if stage:
    #             serializer = StageSerializer(stage, data=request.data, partial=True)
    #             serializer.is_valid(raise_exception=True)
    #             serializer.save()
    #
    #             return Response({"data": serializer.data, "status": status.HTTP_201_CREATED})
    #
    #     return Response({"data": [], "status": status.HTTP_400_BAD_REQUEST})
    #
    # @action(methods=["delete"], detail=True)
    # def delete_stage(self, request, pk=None):
    #     stage_id = request.GET.get("pk")
    #
    #     if stage_id:
    #         stage = Stage.objects.filter(pk=stage_id).first()
    #
    #         if stage:
    #             stage.delete()
    #
    #         return Response({"data": [], "status": status.HTTP_204_NO_CONTENT})
    #
    #     return Response({"data": [], "status": status.HTTP_400_BAD_REQUEST})
    #
    # @method_decorator(cache_page(60 * 60 * 2))
    # @method_decorator(vary_on_cookie)
    # @action(methods=["get"], detail=True)
    # def get_stage(self, request, pk):
    #     stage_id = request.GET.get("pk")
    #
    #     if stage_id:
    #         stage = Stage.objects.filter(pk=stage_id).first()
    #
    #         if stage:
    #             serializer = StageSerializer(stage)
    #
    #             return Response({"data": serializer.data, "status": status.HTTP_204_NO_CONTENT})
    #
    #     return Response({"data": [], "status": status.HTTP_400_BAD_REQUEST})
    #
    # @method_decorator(cache_page(60 * 60 * 2))
    # @method_decorator(vary_on_cookie)
    # @action(methods=["get"], detail=True)
    # def stages(self, request, pk):
    #     goal = self.get_object()
    #
    #     if goal:
    #         queryset = goal.stages.all()
    #
    #         serializer = StageSerializer(queryset, many=True)
    #
    #         return Response({"data": serializer.data, "status": status.HTTP_200_OK})
    #
    #     return Response({"status": status.HTTP_404_NOT_FOUND})


class FileViewSet(
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    permission_classes = [permissions.IsAuthenticated]
    queryset = File.objects.all()
    serializer_class = FileSerializer

