from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from goal.models import Goal, Stage, File


class FileSerializer(ModelSerializer):
    class Meta:
        model = File
        fields = ["id", "file", "stage"]


class StageSerializer(ModelSerializer):
    videos = FileSerializer(many=True, read_only=True)

    class Meta:
        model = Stage
        fields = ["id", "goal", "ti tle", "status", "videos"]

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.status = validated_data.get("status", instance.status)

        instance.save()

        return instance


class GoalSerializer(ModelSerializer):
    stages = StageSerializer(many=True, read_only=True)

    class Meta:
        model = Goal
        fields = [
            "id",
            "title",
            "status",
            "description",
            "deadline",
            "stages",
        ]

