from django.db import models


class Goal(models.Model):
    title = models.CharField(max_length=255)
    deadline = models.DateField()
    description = models.TextField(max_length=1250)
    status = models.BooleanField(default=False)


class Stage(models.Model):
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE, related_name="stages")
    title = models.CharField(max_length=255)
    status = models.BooleanField(default=False)


class File(models.Model):
    file = models.FileField()
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE, related_name="videos")

    